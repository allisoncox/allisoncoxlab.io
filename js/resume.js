/*document ready callback when page loaded
jquery ajax callback to get the data
within that a success and error to get data or show error message
within document ready callback
*/

function documentReadyCb() {
  "use strict";

  $.ajax('skills.json', {
    dataType: 'json',
    success: function(data) {

      data.forEach(function(element) {
        var skillType=element.skillType;

        if (skillType.match(/^(Programming|Web)$/)) {
          $('.ProgWebSkillList').append(createElement(element));
        } else {
          $('.OSOtherSkillList').append(createElement(element));
        }
      });
    },
    error: function(jqXHR, textStatus) {
      console.log(textStatus);
    }

  });
}

function createElement(e) {
  var element = '';

  element += '<li class="list-group-item">';
  element += e.skillName;
  element += '</li>'

  return element;
}

$(document).ready(documentReadyCb);
